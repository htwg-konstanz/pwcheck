#!/usr/bin/env python
# -*- coding: utf-8 -*-
# vim: ts=4 sw=4 et
# check password quality using haveibeenpwned and zxcvbn
import hashlib
import sys
import requests
from zxcvbn import zxcvbn
from zxcvbn.__main__ import JSONEncoder
import argparse
import json
import getpass
import re
import string


# original dropbox score description:
# 0 too guessable: risky password. (guesses < 10^3)
# 1 very guessable: protection from throttled online attacks. (guesses < 10^6)
# 2 somewhat guessable: protection from unthrottled online attacks. (guesses < 10^8)
# 3 safely unguessable: moderate protection from offline slow-hash scenario. (guesses < 10^10)
# 4 very unguessable: strong protection from offline slow-hash scenario. (guesses >= 10^10)
score = {
    0: "very weak",
    1: "weak",
    2: "moderate",
    3: "ok",
    4: "strong"
} 

class PasswordPolicy():
    """settings for password policy"""
    owasp_special = " !\"#$%&'()*+,-./:;<=>?@[]^_`{|}~"
    #valid_special = "+-_=.,"
    #valid_chars = f"{string.ascii_letters}{string.digits}{valid_special}"
    #len_min = 14
    #len_max = 64

    def __init__(self, valid_chars: str = f"{string.ascii_letters}{string.digits}{owasp_special}", len_min: int = 14, len_max: int = 64):
        self.valid_chars = valid_chars
        self.len_min = len_min
        self.len_max = len_max


def get_invalid_chars(pw: str, valid_chars: str) -> (bool, str):
    """Check password for invalid chars
    """
    pattern: re.Pattern = re.compile(f"[^{re.escape(valid_chars)}]")
    invalid_chars: [] = pattern.findall(pw)
    if invalid_chars:
        return(False, f"invalid chars: {str(invalid_chars)[1:-1]}")
    return(True, "has only valid chars")


def check_pw_len(pw: str, len_min: int, len_max: int) -> (bool, str):
    """Check password for correct length
    """
    l = len(pw)
    if l < len_min:
        return(False, f"too short, must be >= {len_min}, is {l}")
    if l > len_max:
        return(False, f"too long, must be <= {len_max}, is {l}")
    return(True, f"has right len: {len_min} <= {l} <= {len_max}")
    


# original code from: https://github.com/mikepound/pwned-search.git
def lookup_pwned_api(pw: str, add_padding: bool = False) -> dict[str: any]:
    """Check password against pwned api

    Args:
        pw: password to check

    Returns:
        A dict {sha1, count, message} where sha1 is SHA-1 hash of pwd, count is number
        of times the password was seen in the pwned database, and message is a message
        string for the user. count equalto 0 indicates that password has not been found.

    Raises:
        RuntimeError: if there was an error trying to fetch data from pwned
            database.
        UnicodeError: if there was an error UTF_encoding the password.
    """
    api: str = 'https://api.pwnedpasswords.com/range'
    sha1pw: str = hashlib.sha1(pw.encode('utf-8')).hexdigest().upper()
    head, tail = sha1pw[:5], sha1pw[5:]
    headers: dict = {}
    if add_padding:
        headers['Add-Padding'] = 'true'
    url: str = "{0}/{1}".format(api,head)
    res: requests.models.Response = requests.get(url, headers=headers)
    if not res.ok:
        raise RuntimeError('Error fetching "{}": {}'.format(url, res.status_code))
    hashes: generator = (line.split(':') for line in res.text.splitlines())
    count: int = next((int(count) for t, count in hashes if t == tail), 0)
    message: str = "password was found {0} times (hash: {1})".format(count, sha1pw)
    return { "sha1pw": sha1pw, "count": count, "message": message }


def check_pw(pw: str, user_input: list = [], add_padding: bool = False, policy: PasswordPolicy = None) -> dict[str: any]:
    """Check pw quality with zxcvbn and haveibeenpwned

    Args:
        pw: password to check

    Returns:
        on success a result dict containing the combined results of 
        zxcvbn and haveibeenpwned. The final password score
        can be extracted from result["score_final"].
    """
    result: dict = { "score_final": None }
    if not pw or pw == '\n':
        return None

    try:
        result: dict = zxcvbn(pw, user_input)
        result["score_final"] = result["score"]
    except IndexError as e:
        print("Error: {0}".format(str(e)), file=sys.stderr) 
    
    try:
        result["pwned"] = lookup_pwned_api(pw, add_padding)
        if result["pwned"]["count"] > 0:
            result["score_final"] = 0
    except (UnicodeError, RuntimeError) as e:
        print("Error: {0}".format(str(e)), file=sys.stderr)

    if policy:
        result["policy"] = {}
        
        invalid_chars: [] = get_invalid_chars(pw, policy.valid_chars)
        result["policy"]["valid_chars"] = invalid_chars[1]
        if not invalid_chars[0]:
            result["score_final"] = 0

        pw_len = check_pw_len(pw, policy.len_min, policy.len_max)
        result["policy"]["valid_length"] = pw_len[1]
        if not pw_len[0]:
            result["score_final"] = 0
            
    result["score_message"] = score[int(result["score_final"])]
    return result


def mainparser() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Check password quality with zxcvbn and haveibeenpwned")
    parser.add_argument('--user-input', action='append',help='add user info to tested dictionary (names, birthdates, ...)')
    parser.add_argument('--add-padding', action='store_true', help='add padding header to haveibeenpwned request (more entropy)')
    parser.add_argument('--nogetpass', action='store_true', help='don\'t use secure getpass to read password (for scripting)')
    parser.add_argument('passwords', metavar='PW', nargs='*', help="password(s) to check")
    args = parser.parse_args()
    return args


def main(args: list) -> int:
    error_code: int = 0
    args: argparse.Namespace = mainparser()
    pw: str = None
    pw_result: dict = None
    #policy = PasswordPolicy(valid_chars=f"{string.ascii_letters}{string.digits}+-_=.,")
    policy = PasswordPolicy()

    try:
        if args.nogetpass or args.passwords:
            print("Exit with <cltr>+c", file=sys.stderr)
            for pw in args.passwords or sys.stdin:
                pw = pw.strip()
                pw_result = check_pw(pw, args.user_input, args.add_padding, policy)
                if pw_result:
                    print(json.dumps(pw_result, indent=2, cls=JSONEncoder))
                else:
                    error_code = 1
        else:
            pw = getpass.getpass()
            pw_result = check_pw(pw, args.user_input, args.add_padding, policy)
            if pw_result:
                print(json.dumps(pw_result, indent=2, cls=JSONEncoder))
            else:
                error_code = 1
    except KeyboardInterrupt:
        print("Bye!", file=sys.stderr) 

    return error_code


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
