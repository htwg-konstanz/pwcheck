# pwcheck  

    Copyright (c) 2023 tzink _at_ htwg-konstanz _dot_ de
    Usage of the works is permitted provided that this instrument is retained with the works, so that any entity that uses the works is notified of this instrument.
    DISCLAIMER: THE WORKS ARE WITHOUT WARRANTY.  
    [https://opensource.org/licenses/Fair/](https://opensource.org/licenses/Fair/)

Check password quality with zxcvbn and haveibeenpwned.

